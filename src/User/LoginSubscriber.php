<?php
declare(strict_types=1);

namespace Lookyman\NetteOAuth2Server\User;

use Lookyman\NetteOAuth2Server\RedirectConfig;
use Nette\Application\AbortException;
use Nette\Application\Application;
use Nette\Application\IPresenter;
use Nette\InvalidStateException;
use Nette\Security\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class LoginSubscriber implements EventSubscriberInterface
{
	/**
	 * @var IPresenter|null
	 */
	private $presenter;

	/**
	 * @var int
	 */
	private $priority;

	/**
	 * @var RedirectConfig
	 */
	private $redirectConfig;

	/**
	 * @param RedirectConfig $redirectConfig
	 * @param int $priority
	 */
	public function __construct(RedirectConfig $redirectConfig, int $priority = 0)
	{
		$this->redirectConfig = $redirectConfig;
		$this->priority = $priority;
	}

	/**
	 * @param Application $application
	 * @param IPresenter $presenter
	 */
	public function onPresenter(Application $application, IPresenter $presenter)
	{
		$this->presenter = $presenter;
	}

	/**
	 * @param User $user
	 * @throws InvalidStateException
	 * @throws AbortException
	 */
	public function onLoggedIn(User $user)
	{
		if (!$this->presenter) {
			throw new InvalidStateException('Presenter not set');
		}
	}

	/**
	 * @return array
	 */
	public static function getSubscribedEvents()
	{
		return [
			Application::class . '::onPresenter',
			User::class . '::onLoggedIn'
		];
	}
}
